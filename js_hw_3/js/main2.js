// Part 1.
// 1. JS is functional language means a function is a primary modular unit of execution.
// With functions the program code becomes reusable: we define a set of certain instructions once,
// and then we can re-use it everywhere we need.
// Using functions instead of duplicating the code at many places decreases dramatically the code size,
// makes it easier to debug and fixing bugs in one place instead of many.
// 2. When we invoke the function we pass the data (values) to a function as arguments.
// Then the function is being executed with the arguments.

"use strict";

let number1;
let number2;
let doAction;

function calculator(number1, number2, doAction) {

    switch (doAction) {
        case "+" :
            return Number(number1) + Number(number2);
        case "-" :
            return number1 - number2;
        case "*" :
            return number1 * number2;
        case "/":
            return number1 / number2;
    }
}

function inputChecker() {

    while (isNaN(number1) || number1 === null) {
        number1 = prompt(`Enter Number 1:`, `0`);
    }
    while (isNaN(number2) || number2 === null) {
        number2 = prompt(`Enter Number 2:`, `0`);
    }
    while (doAction !== "+" && doAction !== "-" && doAction !== "*" && doAction !== "/") {
        doAction = prompt(`Enter operator + , - , * , /`);
    }
}

inputChecker();

console.log(`${number1} ${doAction} ${number2} = ${calculator(number1, number2, doAction)}`);