// Java Script method is a function stored as object property.
// Method contains a function definition.


function createNewUser(firstName, lastName) {

    let newUser = {

        firstName: firstName,
        lastName: lastName,

        read: function () {
            while (/[0-9]/.test(this.firstName) || this.firstName == null || this.firstName === "") {
                this.firstName = prompt("Enter your first name");
            }
            while (/[0-9]/.test(this.lastName) || this.lastName == null || this.lastName === "") {
                this.lastName = prompt("Enter your last name");
            }
        },

        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        }
    };

    newUser.read();
    console.log(newUser.getLogin());

}